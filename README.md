# LaTeX-Sublime-Text-3-snippets
My collection of snippet for my LaTeX project(s).

## How to install :
go to `Sublime Text>Preferences>Browse Packages` and use :

     git clone git@gitlab.com:benjaminBoboul/LaTeX-Sublime-Text-3-snippets.git

A new folder named `LaTeX-Sublime-Text-3-snippets` should appear and the snippets will be available in Sublime Text